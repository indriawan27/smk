@extends('layout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><b>Data Mata Pelajaran</b>
                    <button class="btn btn-success btn-sm btn-flat pull-right" id="tambah"><i
                            class="fa fa-plus"></i>Tambah</button>
                </div>
                <div class="card-body">
                    <table id="tbl_list" class="table table-bordered table-hover dataTable dtr-inline" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nama Mapel</th>
                                <th>Keterangan Mapel</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Mapel</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form name="form_mapel" id="form_mapel" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group"><label class="col-md-4 control-label">Nama Mapel</label>
                        <div class="col-lg-12">
                            <input type="hidden" name="idmapel" id="idmapel">
                            <input type="text" name="nama_mapel" placeholder="Nama Mapel" class="form-control">
                        </div>
                    </div>
                    <div class="form-group"><label class="col-md-4 control-label">Keterangan Mapel</label>
                        <div class="col-lg-12">
                            <input type="text" name="keterangan_mapel" placeholder="Keterangan Mapel"
                                class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" id="saveBtn" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection


@push('js')

<!-- jQuery -->
<script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- DataTables  & Plugins -->
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('assets/plugins/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/plugins/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/dist/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->

<script type="text/javascript">

  $(document).ready(function () { 

    $.ajaxSetup({

          headers: {

              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

          }

    });   

    var table = $('#tbl_list').DataTable({

        processing: true,

        serverSide: true,

        ajax: "{{ route('mapel.datatable') }}",

        type: "GET",

        columns: [

                { data: 'DT_RowIndex'},

                { data: 'nama_mapel'},

                { data: 'keterangan_mapel'},

                { data: 'action'}

            ], 

        

    }); 

     $('#tambah').click(function () {

        $('#saveBtn').val("tambah");

        $('#idmapel').val('');

        $('#form_mapel').trigger("reset"); 

        $('#modal-add').modal('show');  

    });

    $('#saveBtn').click(function (e) {

        e.preventDefault();        

        $(this).html('Menyimpan..');   

        $.ajax({

          data: $('#form_mapel').serialize(),

          url: "{{ url('mapel/store') }}",

          type: "POST",

          dataType: 'json',

          success: function (data) {

              //console.log(data);

              $('#form_mapel').trigger("reset");

              $('#saveBtn').html('Simpan');

              $('#modal-add').modal('hide');

              table.draw();  

              //table.ajax.reload();

          },

          error: function (data) {

              console.log('Error:', data);

              $('#saveBtn').html('Gagal simpan ulang');

          }

      });

    });

    $('body').on('click', '.edit', function () {

        var cek = $(this).attr('data-id');

               $.ajax({                     

                    url : "{{route('mapel.edit')}}?id_mapel="+cek,

                    type: "GET",

                    dataType: "JSON",

                    success: function(data)

                    {       

                        $('#saveBtn').val("edit");          

                        $("#form_mapel").find("input[name=idmapel]").val(data.id_mapel);

                        $("#form_mapel").find("input[name=nama_mapel]").val(data.nama_mapel);

                        $("#form_mapel").find("input[name=keterangan_mapel]").val(data.keterangan_mapel);

                        $("#exampleModalLabel").text("Form Edit Pegawai");

                        $('#modal-add').modal('show');                          

                    }

                });

    });

    $('body').on('click', '.hapus', function () {

        var cek = $(this).attr('data-id');               
        //confirm("Anda yakin akan menghapusnya?");
        
               $.ajax({                   

                    url : "{{url('mapel/destroy')}}/"+cek,

                    type: "POST",

                    success: function(data)

                    {   

                       table.draw();                        

                    }

                }); 

    });

//     $('#date').datepicker({                      

//                 format: 'yyyy-mm-dd',

//                 autoclose: true,

//             }); 

});

</script>
@endpush

@push('css')
    <title>Laravel 6 Ajax CRUD tutorial using Datatable - ItSolutionStuff.com</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('assets/dist/css/adminlte.min.css') }}">
@endpush