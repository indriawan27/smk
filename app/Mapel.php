<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mapel extends Model
{
    //
    protected $table = 'mapel';
    protected $primaryKey = 'id_mapel';
    protected $fillable = ['nama_mapel','keterangan_mapel'];
}
