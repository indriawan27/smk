<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mapel;
use DataTables;
class MapelController extends Controller
{
    //
    public function index(){
        return view ('mapel.index');
    }

    public function datatable(){
        $data = Mapel::latest()->get();
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
        $aksi = ' 
        <a href="javascript:void(0)" data-id="'.$row->id_mapel.'" title="Edit" id="editdata" class="edit btn btn-primary btn-sm">Edit</a>
        <a href="javascript:void(0)" data-id="'.$row->id_mapel.'" title="Delete" class="hapus btn btn-danger btn-sm">Hapus</a>
        ';
        return $aksi;
        })
        ->rawColumns(['action'])
        ->make(true); 
    }
    
    public function store(Request $request){
        // dd($_POST['idmapel']);
        Mapel::updateOrCreate(
            
                ['id_mapel' => $request->idmapel],

                [
                  
                    'nama_mapel'  => $request->nama_mapel,

                    'keterangan_mapel' => $request->keterangan_mapel

                ]

            ); 

        return response()->json(['success'=>'Saved successfully.']);

    }

    public function edit(Request $request){
        $data = Mapel::findOrFail($request->get('id_mapel')); //id mbuh entuk soko ngendi
        echo json_encode($data);
    }

    public function update(Request $request, Mapel $mapel)

    {

        //

    }
    

    public function destroy($id_mapel){
        Mapel::where('id_mapel',$id_mapel)->delete();
    }
}
