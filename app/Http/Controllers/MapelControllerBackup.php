<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mapel;
use DataTables;

class MapelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
   
        if ($request->ajax()) {
            $data = Mapel::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = '<button onclick="edit('.$row->id_mapel.')" class="edit btn btn-primary btn-sm">Edit</button>';
                        $btn = $btn.'<button onclick="hapus('.$row->id_mapel.')" class="hapus btn btn-danger btn-sm">Hapus</button>';  
                        
                       // $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id_mapel.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editMapel">Edit</a>';
   
                        //   $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id_mapel.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteMapel">Delete</a>';
    
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
      
        return view('mapel.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        Mapel::updateOrCreate(['id_mapel' => $request->mapel_id],
                ['nama_mapel' => $request->nama_mapel, 'keterangan_mapel' => $request->keterangan_mapel]);        
   
        return response()->json(['success'=>'Product saved successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_mapel)
    {
        //

        $mapel = Mapel::find($id_mapel);
        return response()->json($mapel);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_mapel)
    {
        //

        Mapel::find($id_mapel)->delete();
        return response()->json(['success'=>'Mapel Berhasil Dihapus.']);
    }
}
