<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// route::resource('mapel','MapelController');

// Route::get('/coba', function () {
//     return view('mapel.index');
// });

// Route::resource('ajaxproducts','ProductAjaxController');

Route::get('/mapel', 'MapelController@index')->name('mapel.index');
Route::get('/mapel/datatable', 'MapelController@datatable')->name('mapel.datatable');
Route::post('/mapel/store', 'MapelController@store')->name('mapel.store');
Route::get('/mapel/edit', 'MapelController@edit')->name('mapel.edit');
Route::post('/mapel/destroy/{id_mapel}', 'MapelController@destroy');